<?php

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler('main', 'OnBuildGlobalMenu', 'owlMenuHandler');

function owlMenuHandler(&$globalMenu, &$moduleMenu)
{
    
    $moduleMenu[] = [
        'parent_menu' => 'global_menu_services',
        'section' => 'owl_blog_comments',
        'sort' => 1250,
        'text' => Loc::getMessage('OWL_BLOG_COMMENTS'),
        'title' => Loc::getMessage('OWL_BLOG_COMMENTS'),
        'icon' => '',
        'page_icon' => '',
        'items_id' => 'menu_owl_blog_comments',
        'items' => [
            [
                'text' => Loc::getMessage('OWL_BLOG_COMMENTS_LIST'),
                'title' => Loc::getMessage('OWL_BLOG_COMMENTS_LIST'),
                'url' => OWL_MID . '_' . 'review-list.php' . '?lang=' . LANG,
                'more_url' => [
                ],
            ],
        ],
    ];
}