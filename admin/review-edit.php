<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

use \CAdminTabControl as Tabs;
use \CAdminContextMenu as Menu;
use \CAdminMessage as Message;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\HttpApplication as App;
use \Bitrix\Main\Loader;
use \Owl\Core\ReviewTable;

Loc::loadMessages(__FILE__);

/** @global CMain $APPLICATION */

Loader::includeModule('owl.core');

CJSCore::Init(['jquery', 'date']);

\Bitrix\Main\UI\Extension::load('ui.buttons.icons');

$rights = $APPLICATION->GetGroupRight(OWL_MID);

if ($rights === 'D') {
    $APPLICATION->AuthForm(Loc::getMessage('ACCESS_DENIED'));
}

$haveAccess = $rights === 'W';

$request = App::getInstance()->getContext()->getRequest();

$id = intval($request['ID']);
$edit = $id > 0;
$post = $request->isPost();
$save = $request->get('save');
$apply = $request->get('apply');

$listPage = OWL_MID . '_review-list.php';
$editPage = $APPLICATION->GetCurPage();
$lang = LANG;

$culture = \Bitrix\Main\Context::getCurrent()->getCulture();

$contextMenu = [
    [
        'TEXT' => Loc::getMessage('OWL_REVIEW_LIST'),
        'TITLE' => Loc::getMessage('OWL_REVIEW_LIST'),
        'LINK' => "{$listPage}?lang={$lang}",
        'ICON' => 'btn_list',
    ],
];
$entityFields = [
    'COMMENT',
    'MODERATED',
    'DATE_ADD',
    'PARENT_ID',
    'RATING',
    'PAGE_INDEX',
    'USER_NAME',
    'USER_EMAIL',
];
// Если в данный момент мы обрабатываем запрос на добавление/редактирование
if ($post && ($save || $apply) && $haveAccess && check_bitrix_sessid()) {
    $requestData = [];
    foreach ($entityFields as $fieldName) {
        if ($fieldName == 'DATE_ADD') continue;
        $requestData[$fieldName] = $request->get($fieldName);
        
        // Обработка специфичных полей, типа чекбокса и тому подобных
        if ($fieldName == 'MODERATED') {
            if ($requestData[$fieldName] == 'on') {
                $requestData[$fieldName] = 1;
            } else {
                $requestData[$fieldName] = 0;
            }
        }
        if ($fieldName == 'PARENT_ID' && !$requestData[$fieldName]) {
            $requestData[$fieldName] = 0;
        }
    }
    
    $dateCreated = \DateTime::createFromFormat(\Bitrix\Main\Type\DateTime::getFormat($culture), $request->get('DATE_ADD'));
    
    if ($dateCreated) {
        $requestData['DATE_ADD'] = \Bitrix\Main\Type\DateTime::createFromPhp($dateCreated);
    }
    
    if ($edit) {
        $result = ReviewTable::update($id, $requestData);
    } else {
        $result = ReviewTable::add($requestData);
    }
    
    if ($result->isSuccess()) {
        $error = false;
        
        if ($save) {
            LocalRedirect("{$listPage}?lang={$lang}");
        } else {
            $id = $result->getId();
            $type = $edit ? 'edit' : 'add';
            
            LocalRedirect("{$editPage}?ID={$id}&lang={$lang}&MESSAGE=ok&TYPE={$type}");
        }
    } else {
        $errors = $result->getErrorMessages();
    }
}

if ($save || $apply) {
    $entity = $requestData;
} elseif ($edit) {
    $entity = ReviewTable::getRowById($id);
} else {
    $entity = [];
}

if ($edit) {
    $APPLICATION->SetTitle(Loc::getMessage('OWL_REVIEW_EDIT'));
    
    $contextMenu[] = [
        'TEXT' => Loc::getMessage('OWL_REVIEW_ADD'),
        'TITLE' => Loc::getMessage('OWL_REVIEW_ADD'),
        'LINK' => "{$editPage}?lang={$lang}",
        'ICON' => 'btn_new',
    ];
} else {
    $APPLICATION->SetTitle(Loc::getMessage('OWL_REVIEW_ADD'));
}

require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';

// Подготовка формы

$contextMenu = new Menu($contextMenu);
$contextMenu->Show();

if ($request['MESSAGE'] === 'ok' && $edit) {
    if ($request['TYPE'] === 'edit') {
        $text = Loc::getMessage('OWL_REVIEW_SUCCESS_EDITED');
    } else {
        $text = Loc::getMessage('OWL_REVIEW_SUCCESS_ADDED');
    }
    
    Message::ShowMessage([
        'MESSAGE' => $text,
        'TYPE' => 'OK',
    ]);
}

if ($errors) {
    $text = [];
    $fieldNameRegex = '/"(.+)"/';
    $number = 1;
    $errorsCount = count($errors);
    foreach ($errors as $error) {
        $errorText = $error;
        
        if (preg_match($fieldNameRegex, $error, $matches)) {
            $fieldName = $matches[1];
            $errorText = preg_replace("/{$fieldName}/u", Loc::getMessage('OWL_REVIEW_' . $fieldName . '_TITLE'), $error);
        }
        
        if ($errorsCount > 1) {
            $text[] = "{$number}. {$errorText}";
        } else {
            $text[] = $errorText;
        }
        $number++;
    }
    
    Message::ShowMessage(implode(PHP_EOL, $text));
}
$allTabs = [];
$allTabs[] = [
    'DIV' => 'settings',
    'TAB' => Loc::getMessage('OWL_REVIEW_SETTINGS'),
    'TITLE' => Loc::getMessage('OWL_REVIEW_SETTINGS'),
];

$tabs = new Tabs('tabControl', $allTabs);
// Простая форма с данными текущего элемента
?>
    <form action="<?=$editPage?>" method="post">
        <?php
        echo bitrix_sessid_post();
        echo '<input type="hidden" name="ID" value="' . intval($id) . '"/>';
        
        $tabs->Begin();
        
        $tabs->BeginNextTab();
        
        ?>
        <?php
        $fieldName = 'COMMENT';
        $val = $entity[$fieldName] ?? '';
        ?>
        <div class="field">
            <div class="field-label"><?=Loc::getMessage('OWL_REVIEW_' . $fieldName . '_TITLE')?></div>
            <div class="field-input">
                <textarea name="<?=$fieldName?>"><?=$val?></textarea>
            </div>
        </div>
    
        <?php
        $fieldName = 'DATE_ADD';
        $val = $entity[$fieldName] ?? '';
        ?>
        <div class="field">
            <div class="field-label"><?=Loc::getMessage('OWL_REVIEW_' . $fieldName . '_TITLE')?></div>
            <div class="field-input">
                <input readonly type="text" name="<?=$fieldName?>" value="<?=$val?>"
                       onclick="BX.calendar({node: this, field: this, bTime: false});" />
            </div>
        </div>
        
        <?php
        $fieldName = 'RATING';
        $val = $entity[$fieldName] ?? '';
        ?>
        <div class="field">
            <div class="field-label"><?=Loc::getMessage('OWL_REVIEW_' . $fieldName . '_TITLE')?></div>
            <div class="field-input">
                <input type="number" min="1" max="5" step="1" name="<?=$fieldName?>" value="<?=$val?>" />
            </div>
        </div>
    
        <?php
        $fieldName = 'MODERATED';
        $val = $entity[$fieldName] ? 'checked' : '';
        ?>
        <div class="field">
            <div class="field-label"><?=Loc::getMessage('OWL_REVIEW_' . $fieldName . '_TITLE')?></div>
            <div class="field-input">
                <input <?=$val?> name="<?=$fieldName?>" type="checkbox" />
            </div>
        </div>
    
        <?php
        $fieldName = 'USER_NAME';
        $val = $entity[$fieldName] ?? '';
        ?>
        <div class="field">
            <div class="field-label"><?=Loc::getMessage('OWL_REVIEW_' . $fieldName . '_TITLE')?></div>
            <div class="field-input">
                <input type="text" name="<?=$fieldName?>" value="<?=$val?>" />
            </div>
        </div>
        
        <?php
        $fieldName = 'USER_EMAIL';
        $val = $entity[$fieldName] ?? '';
        ?>
        <div class="field">
            <div class="field-label"><?=Loc::getMessage('OWL_REVIEW_' . $fieldName . '_TITLE')?></div>
            <div class="field-input">
                <input type="text" name="<?=$fieldName?>" value="<?=$val?>" />
            </div>
        </div>
        
        <?php
        $fieldName = 'PAGE_INDEX';
        $val = $entity[$fieldName] ?? '';
        ?>
        <div class="field">
            <div class="field-label"><?=Loc::getMessage('OWL_REVIEW_' . $fieldName . '_TITLE')?></div>
            <div class="field-input">
                <input type="text" name="<?=$fieldName?>" value="<?=$val?>" />
            </div>
        </div>
        
        <?php
        $fieldName = 'PARENT_ID';
        $val = $entity[$fieldName] ?? '';
        ?>
        <div class="field">
            <div class="field-label"><?=Loc::getMessage('OWL_REVIEW_' . $fieldName . '_TITLE')?></div>
            <div class="field-input">
                <input type="text" name="<?=$fieldName?>" value="<?=$val?>" />
            </div>
        </div>
        
        <?php
        
        $tabs->Buttons([
            'back_url' => "/bitrix/admin/{$listPage}?lang={$lang}",
        ]);
        
        $tabs->End();
        ?>
    </form>
<?php

require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php';