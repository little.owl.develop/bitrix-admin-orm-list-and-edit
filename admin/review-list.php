<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

/** @global CMain $APPLICATION */

use Bitrix\Main\Localization\Loc;
use Bitrix\Iblock\PropertyEnumerationTable;
use Bitrix\Main\Grid\Options as GridOptions;
use Bitrix\Main\UI\PageNavigation;

Loc::loadMessages(__FILE__);

$APPLICATION->SetTitle(Loc::getMessage('OWL_REVIEW_LIST'));

$rights = $APPLICATION->GetGroupRight(OWL_MID);

if ($rights === 'D') {
    $APPLICATION->AuthForm(Loc::getMessage('ACCESS_DENIED'));
}

$haveAccess = $rights === 'W';

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';

$listId = 'review_list';

$gridOptions = new GridOptions($listId);
$sort = $gridOptions->GetSorting(['sort' => ['ID' => 'DESC'], 'vars' => ['by' => 'by', 'order' => 'order']]);
$navParams = $gridOptions->GetNavParams();

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

if ($request->get('op') == 'delete' && $request->get('ID')) {
    $delete = \Owl\Core\ReviewTable::delete($request->get('ID'));
    if (!$delete->isSuccess()) {
        $errors = $delete->getErrorMessages();
    }
}


$nav = new PageNavigation('request_list');
$nav->allowAllRecords(true)
    ->setPageSize($navParams['nPageSize'])
    ->initFromUri();

$filterOption = new Bitrix\Main\UI\Filter\Options($listId);
$filterData = $filterOption->getFilter([]);
$filter = [];

if ($filterData['FILTER_APPLIED']) {
    if ($filterData['FIND']) {
        $filter['COMMENT'] = '%' . $filterData['FIND'] . '%';
    }
    
    if ($filterData['MODERATED']) {
        $filter['=MODERATED'] = $filterData['MODERATED'] == 'Y' ? 1 : '';
    }
    
    if ($filterData['RATING']) {
        $filter['=RATING'] = $filterData['RATING'];
    }
    
    if ($filterData['DATE_ADD_from'] && $filterData['DATE_ADD_to']) {
        $filter['>=DATE_ADD'] = $filterData['DATE_ADD_from'];
        $filter['<=DATE_ADD'] = $filterData['DATE_ADD_to'];
    }
}

$res = \Owl\Core\ReviewTable::getList([
    'filter' => $filter,
    'select' => [
        '*',
    ],
    'offset' => $nav->getOffset(),
    'limit' => $nav->getLimit(),
    'order' => $sort['sort'],
]);

$uiFilter = [
    ['id' => 'COMMENT', 'name' => Loc::getMessage('OWL_REVIEW_' . 'COMMENT' . '_TITLE'), 'type' => 'text', 'default' => true],
    ['id' => 'MODERATED', 'name' => Loc::getMessage('OWL_REVIEW_' . 'MODERATED' . '_TITLE'), 'type' => 'checkbox', 'default' => true],
    [
        'id' => 'RATING', 'name' => Loc::getMessage('OWL_REVIEW_' . 'RATING' . '_TITLE'), 'type' => 'list', 'default' => true,
        'items' => [
            '', '1', '2', '3', '4', '5'
        ]
    ],
    ['id' => 'DATE_ADD', 'name' => Loc::getMessage('OWL_REVIEW_' . 'DATE_ADD' . '_TITLE'), 'type' => 'date', 'default' => true],
];

$APPLICATION->IncludeComponent('bitrix:main.ui.filter', '', [
    'FILTER_ID' => $listId,
    'GRID_ID' => $listId,
    'FILTER' => $uiFilter,
    'ENABLE_LIVE_SEARCH' => true,
    'ENABLE_LABEL' => true,
]);

$columns = [];
$columns[] = ['id' => 'ID', 'name' => Loc::getMessage('OWL_REVIEW_' . 'ID' . '_TITLE'), 'sort' => 'ID', 'default' => true];
$columns[] = ['id' => 'DATE_ADD', 'name' => Loc::getMessage('OWL_REVIEW_' . 'DATE_ADD' . '_TITLE'), 'sort' => 'DATE_ADD', 'default' => true];
$columns[] = ['id' => 'MODERATED', 'name' => Loc::getMessage('OWL_REVIEW_' . 'MODERATED' . '_TITLE'), 'sort' => 'MODERATED', 'default' => true];
$columns[] = ['id' => 'COMMENT', 'name' => Loc::getMessage('OWL_REVIEW_' . 'COMMENT' . '_TITLE'), 'sort' => 'COMMENT', 'default' => true];
$columns[] = ['id' => 'RATING', 'name' => Loc::getMessage('OWL_REVIEW_' . 'RATING' . '_TITLE'), 'sort' => 'RATING', 'default' => true];

$allItems = $res->fetchAll();

$currentDir = $APPLICATION->GetCurDir();
$listPage = $APPLICATION->GetCurPage();
$editPage = $currentDir . OWL_MID . '_review-edit.php';

foreach ($allItems as $row) {
    // var_dump($row);
    $list[] = [
        'data' => [
            'ID' => $row['ID'],
            'DATE_ADD' => $row['DATE_ADD'],
            'COMMENT' => $row['COMMENT'],
            'MODERATED' => Loc::getMessage('OWL_REVIEW_CHECKBOX_' . $row['MODERATED']),
            'RATING' => $row['RATING'],
        ],
        'actions' => [
            [
                'text' => Loc::getMessage('OWL_REVIEW_ACTION_VIEW'),
                'default' => true,
                'onclick' => 'document.location.href="' . $editPage . '?ID=' . $row['ID'] . '"',
            ], [
                'text' => Loc::getMessage('OWL_REVIEW_ACTION_DELETE'),
                'default' => true,
                'onclick' => 'if(confirm("' . Loc::getMessage('OWL_REVIEW_CONFIRM_DELETE') . '")){document.location.href="' . $listPage . '?op=delete&ID=' . $row['ID'] . '"}',
            ],
        ],
    ];
}

$APPLICATION->IncludeComponent('bitrix:main.ui.grid', '', [
    'GRID_ID' => $listId,
    'COLUMNS' => $columns,
    'ROWS' => $list,
    'SHOW_ROW_CHECKBOXES' => false,
    'NAV_OBJECT' => $nav,
    'AJAX_MODE' => 'Y',
    'AJAX_ID' => \CAjax::getComponentID('bitrix:main.ui.grid', '.default', ''),
    'PAGE_SIZES' => [
        ['NAME' => '20', 'VALUE' => '20'],
        ['NAME' => '50', 'VALUE' => '50'],
        ['NAME' => '100', 'VALUE' => '100'],
    ],
    'AJAX_OPTION_JUMP' => 'N',
    'SHOW_CHECK_ALL_CHECKBOXES' => false,
    'SHOW_ROW_ACTIONS_MENU' => true,
    'SHOW_GRID_SETTINGS_MENU' => true,
    'SHOW_NAVIGATION_PANEL' => true,
    'SHOW_PAGINATION' => true,
    'SHOW_SELECTED_COUNTER' => true,
    'SHOW_TOTAL_COUNTER' => true,
    'SHOW_PAGESIZE' => true,
    'SHOW_ACTION_PANEL' => true,
    'ALLOW_COLUMNS_SORT' => true,
    'ALLOW_COLUMNS_RESIZE' => true,
    'ALLOW_HORIZONTAL_SCROLL' => true,
    'ALLOW_SORT' => true,
    'ALLOW_PIN_HEADER' => true,
    'AJAX_OPTION_HISTORY' => 'N',
]);

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php';