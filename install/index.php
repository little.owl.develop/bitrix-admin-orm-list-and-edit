<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Bitrix\Main\Entity\Base;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\IO\File;

class owl_core extends CModule
{
    public $MODULE_ID = 'owl.core';
    public $PARTNER_URI = '';
    public $PARTNER_NAME = '';

    public function __construct()
    {
        $arModuleVersion = [];
        $versionPath = dirname(__FILE__);
        include ($versionPath . '/version.php');
        if (!empty($arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
        $this->MODULE_NAME = 'Owl review example';
    
        $this->exclusionAdminFiles = [
            '..',
            '.',
            'menu.php',
        ];
    }

    function DoInstall()
    {
        RegisterModule($this->MODULE_ID);
        Loader::includeModule($this->MODULE_ID);
        $this->installFiles();
        
        $this->createTable('\Owl\Core\ReviewTable');
    }

    function DoUninstall()
    {
        $this->uninstallFiles();
        
        $this->dropTable('\Owl\Core\ReviewTable');
        
        UnRegisterModule($this->MODULE_ID);
    }
    
    function createTable($className)
    {
        $instance = Base::getInstance($className);
        /* @var $class \Bitrix\Main\Entity\DataManager */
        $class = new $className;
        if (!Application::getConnection($class::getConnectionName())->isTableExists($instance->getDBTableName())) {
            $instance->createDbTable();
        }
    }
    
    function dropTable($className) {
        $instance = Base::getInstance($className);
        /* @var $class \Bitrix\Main\Entity\DataManager */
        $class = new $className;
        if (Application::getConnection($class::getConnectionName())->isTableExists($instance->getDBTableName())) {
            Application::getConnection($class::getConnectionName())->dropTable($instance->getDBTableName());
        }
    }
    
    function installFiles()
    {
        $moduleAbsPath = $this->getModulePath();
        $moduleRelativePath = $this->getModulePath(false);
        $bitrixAdminDir = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin';
        
        // Если в модуле есть административные страницы
        if (Directory::isDirectoryExists($moduleAbsPath . '/admin')) {
            // Копируем их из специальной директории в install
            CopyDirFiles($moduleAbsPath . '/install/admin/', $bitrixAdminDir);
            
            // Помещать в install не обязательно, папка admin в корне модуля будет просканирована и файлы
            // будут созданы автоматически
            if ($dir = opendir($moduleAbsPath . '/admin')) {
                while (false !== $file = readdir($dir)) {
                    if (in_array($file, $this->exclusionAdminFiles)) {
                        continue;
                    } else {
                        $fileToCreate = $bitrixAdminDir . '/';
                        $fileToCreate .= $this->MODULE_ID . '_' . $file;
                        $require = '<?php require $_SERVER[\'DOCUMENT_ROOT\'] . \'' . $moduleRelativePath . '/admin/' . $file . '\';';
                        file_put_contents($fileToCreate, $require);
                    }
                }
                closedir($dir);
            }
        }
        
        if (Directory::isDirectoryExists($moduleAbsPath . '/install/themes')) {
            CopyDirFiles($moduleAbsPath . "/install/themes/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/themes", true, true);
        }
    }
    
    function uninstallFiles()
    {
        
        $moduleAbsPath = $this->getModulePath();
        $bitrixAdminDir = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin';
        
        if (Directory::isDirectoryExists($moduleAbsPath . '/admin')) {
            DeleteDirFiles($moduleAbsPath . '/install/admin/', $bitrixAdminDir);
            
            if ($dir = opendir($moduleAbsPath . '/admin')) {
                while (false !== $file = readdir($dir)) {
                    if (in_array($file, $this->exclusionAdminFiles)) {
                        continue;
                    } else {
                        File::deleteFile($bitrixAdminDir . '/' . $this->MODULE_ID . '_' . $file);
                    }
                }
                closedir($dir);
            }
        }
    }
    
    /**
     * Возвращает путь до модуля
     * @return string - путь до модуля
     */
    private static function getModulePath($absolute = true)
    {
        $path = dirname(__DIR__);
        $path = str_replace('\\', '/', $path);
        
        if ($absolute === true) {
            return $path;
        } else {
            return str_replace(Application::getDocumentRoot(), '', $path);
        }
    }
}