<?php
$MESS['OWL_REVIEW_LIST'] = 'Owl список отзывов';
$MESS['OWL_REVIEW_EDIT'] = 'Owl редактировать отзыв';
$MESS['OWL_REVIEW_ADD'] = 'Owl добавить отзыв';
$MESS['OWL_REVIEW_CONFIRM_DELETE'] = 'Удалить?';

$MESS['OWL_REVIEW_SUCCESS_EDITED'] = 'Отзыв успешно изменён';
$MESS['OWL_REVIEW_SUCCESS_ADDED'] = 'Отзыв успешно добавлен';

$MESS['OWL_REVIEW_ACTION_VIEW'] = 'Просмотр';
$MESS['OWL_REVIEW_ACTION_DELETE'] = 'Удалить';

$MESS['OWL_REVIEW_SETTINGS'] = 'Поля отзыва';

// $MESS['OWL_REVIEW_CHECKBOX_' . ''] = 'Не определено';
$MESS['OWL_REVIEW_CHECKBOX_' . ''] = 'Нет';
$MESS['OWL_REVIEW_CHECKBOX_' . '1'] = 'Да';
// $MESS['OWL_REVIEW_CHECKBOX_' . 0] = 'Нет';

$MESS['OWL_REVIEW_' . 'ID' . '_TITLE'] = 'ID';
$MESS['OWL_REVIEW_' . 'DATE_ADD' . '_TITLE'] = 'Создано';
$MESS['OWL_REVIEW_' . 'MODERATED' . '_TITLE'] = 'Модерировано';
$MESS['OWL_REVIEW_' . 'COMMENT' . '_TITLE'] = 'Комментарий';
$MESS['OWL_REVIEW_' . 'RATING' . '_TITLE'] = 'Рейтинг';
$MESS['OWL_REVIEW_' . 'PAGE_INDEX' . '_TITLE'] = 'ID элемента';
$MESS['OWL_REVIEW_' . 'PARENT_ID' . '_TITLE'] = 'Родительский комментарий';
$MESS['OWL_REVIEW_' . 'USER_NAME' . '_TITLE'] = 'Имя автора';
$MESS['OWL_REVIEW_' . 'USER_EMAIL' . '_TITLE'] = 'Email автора';