<?php


namespace Owl\Core;


use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class ReviewTable extends Entity\DataManager
{
    
    /**
     * @return string
     */
    public static function getTableName()
    {
        return 'owl_review';
    }
    
    /**
     * @return array|\Bitrix\Main\Entity\*
     * @throws \Exception
     */
    public static function getMap()
    {
        return [
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
            ]),
            new Entity\StringField('PAGE_INDEX', [
                'required' => true,
            ]),
            new Entity\IntegerField('PARENT_ID', [
                'default_value' => 0,
                'required' => true,
            ]),
            new Entity\DatetimeField('DATE_ADD', [
                'default_value' => new Type\DateTime(),
                'required' => true,
            ]),
            new Entity\IntegerField('RATING', [
                'default_value' => 0,
                'validation' => function () {
                    return [
                        new Entity\Validator\Range(1, 5),
                    ];
                },
            ]),
            new Entity\IntegerField('UP_COUNT', [
                'default_value' => 0,
            ]),
            new Entity\IntegerField('DOWN_COUNT', [
                'default_value' => 0,
            ]),
            new Entity\TextField('COMMENT', [
                'required' => true,
                'validation' => function () {
                    return [
                        new Entity\Validator\Length(5),
                    ];
                },
                'save_data_modification' => function () {
                    return [
                        function ($value) {
                            return htmlspecialcharsbx($value);
                        },
                    ];
                },
            ]),
            new Entity\StringField('USER_NAME', [
                'required' => true,
                'validation' => function () {
                    return [
                        new Entity\Validator\Length(3),
                    ];
                },
                'save_data_modification' => function () {
                    return [
                        function ($value) {
                            return htmlspecialcharsbx($value);
                        },
                    ];
                },
            ]),
            new Entity\IntegerField('USER_PHOTO'),
            new Entity\StringField('USER_EMAIL', [
                'required' => true,
                'validation' => function () {
                    return [
                        new Entity\Validator\Length(5),
                    ];
                },
                'save_data_modification' => function () {
                    return [
                        function ($value) {
                            return htmlspecialcharsbx($value);
                        },
                    ];
                },
            ]),
            new Entity\StringField('USER_IP', [
                'required' => true,
            ]),
            new Entity\BooleanField('MODERATED'),
            new Entity\TextField('ADDITIONAL_PARAMS', [
                'serialized' => true,
            ]),
        ];
    }
    
    /**
     * @param $reviewId
     * @param string $direct
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function updateVote($reviewId, $direct = 'up')
    {
        $res = self::getList([
            'filter' => [
                'ID' => $reviewId,
            ],
            'select' => [
                'UP_COUNT',
                'DOWN_COUNT',
            ],
        ])->fetch();
        if ($res) {
            if ($direct == 'down') {
                self::update($reviewId, [
                    'DOWN_COUNT' => $res['DOWN_COUNT'] + 1,
                ]);
            } else {
                self::update($reviewId, [
                    'UP_COUNT' => $res['UP_COUNT'] + 1,
                ]);
            }
            return true;
        } else {
            return false;
        }
    }
}